## Dependencies.
To deply to Heroku, we need `govendor`.
Install with `sudo apt install govendor` then run `govendor init` in the
project directory.

## Executing
To run, run `go run server.go`

This will start a webserver that you can access from your brwoser at `localhost:8080`

## Heroku Setup.
[https://devcenter.heroku.com/articles/getting-started-with-go#set-up](Here are the directions for setting up and using Heroku to host your Go applicaiton)

### Heroku staging server
#### Heruku Git URL
https://git.heroku.com/go-web-staging.git
#### Deployed URL
https://go-web-staging.herokuapp.com
