package main

import (
	"html/template"
	"log"
	"net/http" // This imports the web stuff.
	"os"
	"time"
)

func getPort() string {
	p := os.Getenv("PORT")

	if p != "" {
		return ":" + p
	}

	return ":8080"
}

type PageVariables struct {
	Date string
	Time string
}

func HomePage(w http.ResponseWriter, r *http.Request) {
	now := time.Now() // Get the current time.
	HomePageVars := PageVariables{
		Date: now.Format("02-01-2006"), // This is just the format. The actual date is now()
		Time: now.Format("15:04:05"),   // This is just the format. The actual time is now()
	}

	// Parse homepage.html
	t, err := template.ParseFiles("homepage.html")
	if err != nil {
		log.Print("template parsing error: ", err)
	}

	// Execute the template with the HomePageVars struct to set the data and
	// time variables in the HTML.
	t.Execute(w, HomePageVars)
}

func main() {
	// This function takes care of handling requests made to "/"
	// by passing the request on to the HomePage function
	http.HandleFunc("/", HomePage)

	// This starts a web server. Easy!!
	http.ListenAndServe(getPort(), nil)
}
